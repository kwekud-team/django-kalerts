import uuid
from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from kalerts.constants import KalertsK
from kalerts.managers import CategoryManager, IncidentManager


class BaseModel(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


SERVER_TYPES = (
    (KalertsK.BACKEND_PAGER_DUTY.value, KalertsK.BACKEND_PAGER_DUTY.value),
)


class RemoteUser(BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    remote_id = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.site}: {self.user}'

    class Meta:
        unique_together = ['site', 'remote_id']


class ProxyUser(BaseModel):
    remote_user = models.ForeignKey(RemoteUser, on_delete=models.CASCADE)
    linked_users = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return str(self.remote_user)

    class Meta:
        unique_together = ['site', 'remote_user']

    def get_linked_user_count(self):
        return self.linked_users.count()
    get_linked_user_count.short_description = 'Linked User count'


CATEGORY_TYPES = (
    (KalertsK.CAT_SERVICE.value, KalertsK.CAT_SERVICE.value),
    (KalertsK.CAT_PRIORITY.value, KalertsK.CAT_PRIORITY.value),
)


class Category(BaseModel):
    type = models.CharField(max_length=50, choices=CATEGORY_TYPES)
    name = models.CharField(max_length=250)
    identifier = models.CharField(max_length=100)
    is_visible = models.BooleanField(default=True)
    sort = models.PositiveSmallIntegerField(default=1000)
    description = models.CharField(max_length=250, null=True, blank=True)
    remote_id = models.CharField(max_length=100, null=True, blank=True)
    remote_name = models.CharField(max_length=250, null=True, blank=True)

    objects = CategoryManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'
        unique_together = ['site', 'type', 'identifier']
        ordering = ('sort',)


class Incident(BaseModel):
    guid = models.UUIDField()
    service = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='incident_service',
                                limit_choices_to={'type': KalertsK.CAT_SERVICE.value})
    title = models.CharField(max_length=250)
    type = models.CharField(max_length=50)
    body = models.TextField(null=True, blank=True)
    urgency = models.CharField(max_length=50)
    priority = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True,
                                 related_name='incident_priority',
                                 limit_choices_to={'type': KalertsK.CAT_PRIORITY.value})

    ref_object_pk = models.CharField(max_length=100)
    ref_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    ref_content_object = GenericForeignKey(ct_field='ref_content_type', fk_field='ref_object_pk')

    incident_number = models.PositiveBigIntegerField(default=0)
    incident_key = models.CharField(max_length=250)

    from_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    remote_server = models.ForeignKey('kservers.Server', on_delete=models.CASCADE, null=True, blank=True)
    remote_id = models.CharField(max_length=250, null=True, blank=True)
    remote_date_resolved = models.DateTimeField(null=True, blank=True)
    remote_status = models.CharField(max_length=50, null=True, blank=True)
    last_sync_date = models.DateTimeField(null=True, blank=True)

    status = models.CharField(max_length=50, null=True, blank=True)
    action_url = models.CharField(max_length=250, null=True, blank=True)

    date_acknowledged = models.DateTimeField(null=True, blank=True)
    date_resolved = models.DateTimeField(null=True, blank=True)
    acknowledged_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True,
                                        related_name='incident_acknowledged_by')
    resolved_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True,
                                    related_name='incident_resolved_by')

    objects = IncidentManager()

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ['site', 'incident_key']
        ordering = ('-date_created',)

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        # self.incident_key = self.incident_key or self.guid
        return super().save(*args, **kwargs)


class IncidentUser(BaseModel):
    incident = models.ForeignKey(Incident, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    is_responsible = models.BooleanField(default=False)
    group_label = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f'{self.incident}: {self.user}'
