from kalerts.contrib.alerts.utils import IncidentUtils, AlertUtils
from kalerts.dclasses import IncidentItem


def log_incident(site, incident_item: IncidentItem):
    return IncidentUtils(site).log_incident(incident_item)


def complete_incident(site, incident_keys, user, status):
    return IncidentUtils(site).complete_incident(incident_keys, user, status)


def get_site_alerts(site):
    return AlertUtils(site).get_site_alerts()
