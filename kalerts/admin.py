from django.contrib import admin
from django.shortcuts import redirect
from django.urls import reverse

from kommons.utils.http import get_request_site
from kalerts.models import RemoteUser, ProxyUser, Category, Incident, IncidentUser
from kalerts.constants import KalertsK
from kalerts.contrib.sync.utils import SyncUtils


@admin.register(RemoteUser)
class RemoteUserAdmin(admin.ModelAdmin):
    list_display = ('remote_id', 'user', 'site')
    list_filter = ('site',)
    search_fields = ['remote_id', 'user__username']


@admin.register(ProxyUser)
class ProxyUserAdmin(admin.ModelAdmin):
    list_display = ('remote_user', 'get_linked_user_count', 'site')
    list_filter = ('site',)
    search_fields = ['remote_user__remote_id']


# def sync_configs(self, obj):
#     url = reverse("kalerts:sync_remote", args=(obj.site.pk,))
#     return mark_safe(f'<a href={url}>Sync</a>')


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'identifier', 'name', 'remote_id', 'remote_name', 'sort', 'site')
    list_filter = ('type', 'is_visible', 'is_active', 'site')
    search_fields = ['name']
    list_editable = ['identifier', 'name', 'remote_id']
    fieldsets = [
        ('Basic', {'fields': ('site', 'type', 'identifier', 'name',)}),
        ('Extra', {'fields': ('sort', 'description', 'remote_id', 'remote_name')}),
    ]


class IncidentUserInline(admin.TabularInline):
    model = IncidentUser
    extra = 1
    fields = ['incident', 'user', 'is_responsible']


@admin.register(Incident)
class IncidentAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'incident_key', 'title', 'service', 'priority', 'urgency', 'status', 'type',
                    'ref_content_object', 'last_sync_date', 'remote_id', 'remote_status', 'site')
    list_filter = ('type', 'service', 'priority', 'urgency', 'site')
    search_fields = ['name']
    inlines = [IncidentUserInline]
    actions = ['sync_local_to_remote', 'sync_remote_to_local']

    def sync_local_to_remote(self, request, queryset):
        SyncUtils().sync_local_to_remote([KalertsK.SYNC_SECTION_INCIDENT.value], queryset)

    def sync_remote_to_local(self, request, queryset):
        site = get_request_site(request)
        if site:
            url = f'{reverse("kalerts:sync_remote", args=(site.pk,))}?section={KalertsK.SYNC_SECTION_INCIDENT.value}'
            return redirect(url)
