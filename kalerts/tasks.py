from celery import shared_task

from kalerts.models import Incident
from kalerts.constants import KalertsK
from kalerts.contrib.sync.utils import SyncUtils


@shared_task
def bgr_sync_local_to_remote(incident_pks):
    incident_qs = Incident.objects.filter(pk__in=incident_pks.split(','))
    res = SyncUtils().sync_local_to_remote([KalertsK.SYNC_SECTION_INCIDENT.value], incident_qs)
    return True  # if res else False
