from typing import List
from dataclasses import dataclass, field
from django.db import models

from kalerts.constants import KalertsK


@dataclass
class AssignmentItem:
    user: models.Model
    is_responsible: bool = False
    group_label: str = ''


@dataclass
class IncidentItem:
    key: str
    title: str
    service_identifier: str
    from_user: models.Model
    type: str = KalertsK.TYPE_INCIDENT.value
    urgency: str = KalertsK.URGENCY_LOW.value
    priority_identifier: str = ''
    description: str = ''
    action_url: str = ''
    ref_instance: models.Model = None
    assignments: List[AssignmentItem] = field(default_factory=list)


@dataclass
class AlertItem:
    key: str
    name: str
    count: int
    queryset: object


@dataclass
class AlertBuilder:
    total_count: int
    alert_items: List[AlertItem] = field(default_factory=list)
