from enum import Enum


class KalertsK(Enum):
    CAT_SERVICE = 'service'
    CAT_PRIORITY = 'priority'

    URGENCY_HIGH = 'high'
    URGENCY_LOW = 'low'

    TYPE_INCIDENT = 'incident'

    STATUS_TRIGGERED = 'triggered'
    STATUS_ACKNOWLEDGED = 'acknowledged'
    STATUS_RESOLVED = 'resolved'

    BACKEND_PAGER_DUTY = 'pager-duty'

    SYNC_SECTION_USER = 'user'
    SYNC_SECTION_SERVICE = 'service'
    SYNC_SECTION_PRIORITY = 'priority'
    SYNC_SECTION_INCIDENT = 'incident'

    SYNC_RUN_VARIABLE = 'KALERTS_SYNC_RUN_TYPE'
    SYNC_RUN_CELERY = 'SYNC_RUN_CELERY'
    SYNC_RUN_REQUEST = 'SYNC_RUN_REQUEST'
    SYNC_RUN_MANAGEMENT = 'SYNC_RUN_MANAGEMENT'
