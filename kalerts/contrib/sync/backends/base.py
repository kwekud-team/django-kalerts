from django.db.models import Q
from django.contrib.auth import get_user_model
from django.utils import timezone

from kalerts.models import Category, Incident
from kalerts.constants import KalertsK
from kservers.dclasses import APIResponse, DetailAPIResponse
from kservers.contrib.requester import Requester


class BaseIntegrator:

    def __init__(self, backend):
        self.backend = backend

    def pull_remote_to_local(self, request_data=None):
        synced = []

        api_response = self.backend.authenticate()
        request_log = self.backend.requester.logger.init_request_log(action='pull_remote_to_local',
                                                                     api_response=api_response)

        if api_response.is_valid:
            list_api_resp = self.get_remote_objects(request_data=request_data)
            api_resp = DetailAPIResponse(**list_api_resp.to_dict())

            for remote_object in list_api_resp.response_body:
                if self.backend.session:
                    obj = self.save_local_object(remote_object)
                    if obj:
                        synced.append(obj)

                        api_resp.response_body = remote_object
                        self.backend.requester.logger.save_request_log_object(request_log, obj, api_resp)

            if list_api_resp.is_valid:
                # Store only count if request is valid. We don't need to store whole list to prevent db bloat
                list_api_resp.response_body = {'count': len(list_api_resp.response_body)}

            self.backend.requester.logger.update_request_log(request_log, api_response=list_api_resp)

        return synced

    def push_local_to_remote(self, queryset):
        synced = []

        api_response = self.backend.authenticate()
        request_log = self.backend.requester.logger.init_request_log(action='push_local_to_remote',
                                                                     api_response=api_response)

        if api_response.is_valid:
            pending_qs = self.get_local_objects(queryset)
            for obj in pending_qs:
                if self.backend.session:
                    api_resp = self.save_remote_object(obj)
                    if api_resp:
                        if api_resp.is_valid:
                            synced.append(api_resp)
                            self.link_local_to_remote(obj, api_resp.remote_id, api_resp.remote_status)

                        self.backend.requester.logger.save_request_log_object(request_log, obj, api_resp)

            self.backend.requester.logger.update_request_log(request_log)

        return synced

    def get_remote_objects(self, request_data=None) -> APIResponse:
        pass

    def save_local_object(self, response_data):
        pass

    def get_local_objects(self, queryset=None, filters=None):
        """
            1. Get incidents that have not been resolved.
            2. Incidents with remote_id defined for service. This means service has been configure to sync remotely
            3. Get incidents with no backend or backend is same as one set in settings. This is to ensure we don't sync
               to wrong backend if backend was changed after some incidents had already been synced.
        """
        filters = filters or {}
        queryset = queryset or Incident.objects.all()
        pending_qs = queryset.filter(remote_date_resolved__isnull=True, **filters)
        return pending_qs.filter(Q(remote_server__isnull=True) | Q(remote_server=self.backend.requester.server))

    def save_remote_object(self, instance) -> APIResponse:
        pass

    def link_local_to_remote(self, instance, remote_id, status):
        remote_date_resolved = (timezone.now() if status == KalertsK.STATUS_RESOLVED.value else None)
        Incident.objects.filter(pk=instance.pk).update(remote_id=remote_id, remote_server=self.backend.requester.server,
                                                       remote_status=status, last_sync_date=timezone.now(),
                                                       remote_date_resolved=remote_date_resolved)


class BaseBackend:
    identifier = 'base'
    name = 'Base'
    base_url = 'http://www.example.com'
    session = None
    integrator_classes = {}

    def __init__(self, site):
        self.site = site
        self.requester = Requester(self.site, self.identifier, base_url=self.base_url)

    def get_integrators(self, sections):
        return {key: sync_cls(self) for key, sync_cls in self.integrator_classes.items() if key in sections}

    def authenticate(self) -> APIResponse:
        session = self.get_session()
        if session:
            self.session = session
            api_response = APIResponse.valid()
        else:
            self.session = None
            api_response = APIResponse.error(response_message='Unauthorized. Please check your api_key')

        return api_response

    def get_session(self):
        pass

    def get_token(self):
        return self.requester.server.settings.get('api_token', '')

    def get_headers(self):
        return {
            'Authorization': f'Token {self.get_token()}',
            'Content-Type': 'application/json'
        }

    def get_base_url(self):
        url = self.requester.server.base_url
        return url[:-1] if url.endswith('/') else url

    def create_category(self, ttype, remote_id, remote_name=''):
        if remote_id:
            category = Category.objects.filter(site=self.site, type=ttype, remote_id=remote_id).first()
            if category:
                Category.objects.filter(pk=category.pk).update(remote_name=remote_name)

            return category

    def create_user(self, username):
        return get_user_model().objects.get_or_create(username=username)[0]

    def get_resource_url(self, url):
        return f'{self.get_base_url()}{url}'
