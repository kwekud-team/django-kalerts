from pdpyras import APISession, PDClientError
from django.template.defaultfilters import slugify
from django.contrib.auth import get_user_model

from kommons.utils.dates import parse_date
from kommons.utils.generic import get_dotted_dict
from kalerts.contrib.sync.backends.base import BaseBackend, BaseIntegrator
from kalerts.constants import KalertsK
from kalerts.models import Category, Incident, IncidentUser, RemoteUser
from kservers.dclasses import APIResponse, ListAPIResponse, DetailAPIResponse


class PDIntegrator(BaseIntegrator):

    def _proccess_exception(self, exception):
        try:
            json_resp = exception.response.json()
            response_code = get_dotted_dict(json_resp, 'error.code')
            errors = get_dotted_dict(json_resp, 'error.errors') or []
            response_message = get_dotted_dict(json_resp, 'error.message')
            response_message += '. ' + ', '.join([x for x in errors])

            response_body = json_resp
        except:
            response_code = 0
            response_message = exception.response.text
            response_body = response_message

        return APIResponse(status_code=exception.response.status_code, is_valid=False, response_code=response_code,
                           response_message=response_message, response_body=response_body)

    def _get_object_list(self, resource, request_data=None):
        api_response = None
        if self.backend.session:
            request_data = request_data or {}
            try:
                incidents = list(self.backend.session.iter_all(resource, params=request_data))
                api_response = ListAPIResponse(status_code=200, response_message='OK', is_valid=True,
                                               response_body=incidents)
            except PDClientError as e:
                api_response = self._proccess_exception(e)

        return api_response


class PDIncidentIntegrator(PDIntegrator):

    def get_remote_objects(self, request_data=None) -> ListAPIResponse:
        return self._get_object_list('incidents')

    def save_local_object(self, remote_object):
        username = get_dotted_dict(remote_object, 'last_status_change_by.id')
        service = self.backend.create_category(
            ttype=KalertsK.CAT_SERVICE.value,
            remote_id=get_dotted_dict(remote_object, 'service.id'),
            remote_name=get_dotted_dict(remote_object, 'service.summary'),
        )
        priority = self.backend.create_category(
            ttype=KalertsK.CAT_PRIORITY.value,
            remote_id=get_dotted_dict(remote_object, 'priority.id'),
            remote_name=get_dotted_dict(remote_object, 'priority.summary'),
        )

        if service:
            incident = Incident.objects.update_or_create(
                site=self.backend.site,
                remote_server=self.backend.server,
                incident_key=remote_object['incident_key'],
                service=service,
                defaults={
                    'from_user': self.backend.create_user(username),
                    'priority': priority,
                    'incident_number': get_dotted_dict(remote_object, 'incident_number'),
                    'remote_id': get_dotted_dict(remote_object, 'id'),
                    'title': get_dotted_dict(remote_object, 'title'),
                    'body': get_dotted_dict(remote_object, 'description', default=''),
                    'type': get_dotted_dict(remote_object, 'type'),
                    'urgency': get_dotted_dict(remote_object, 'urgency'),
                    'status': get_dotted_dict(remote_object, 'status'),
                    'date_modified': parse_date(get_dotted_dict(remote_object, 'last_status_change_at')),
                    'date_created': parse_date(get_dotted_dict(remote_object, 'created_at'))
                }
            )[0]

            for assign_data in get_dotted_dict(remote_object, 'assignments', default=[]):
                username = get_dotted_dict(assign_data, 'assignee.id')

                IncidentUser.objects.get_or_create(
                    site=self.backend.site,
                    incident=incident,
                    user=self.backend.create_user(username),
                    defaults={
                        'date_created': parse_date(get_dotted_dict(assign_data, 'at'))
                    })

            return incident

    def save_remote_object(self, instance) -> DetailAPIResponse:
        payload = {
            "type": instance.type,
            "title": instance.title,
            "service": {"id": instance.service.remote_id, "type": "service_reference"},
            "urgency": instance.urgency,
            "incident_key": instance.incident_key,
            "body": {"type": "incident_body", "details":  instance.body}
            # "assignments": [{"assignee": {"id": "PSFZHTO", "type": "user_reference"}}],
        }
        if instance.priority and instance.priority.remote_id:
            payload['priority'] = {"id": instance.priority.remote_id, "type": "priority_reference"}

        # for assignment in instance.incidentuser_set.all():
        #     payload['priority'] = [{"assignee": {"id": "PSFZHTO", "type": "user_reference"}}]

        try:
            sender_id = self.backend.requester.server.settings.get('sender_id', '')
            # sender_id = 'kdanso@maatecsystems.com'
            self.backend.session.default_from = sender_id
            record = self.backend.session.persist('incidents', 'incident_key', values=payload)

            if instance.status == KalertsK.STATUS_RESOLVED.value:
                payload = [{'id': record['id'], 'type':'incident_reference', 'status': KalertsK.STATUS_RESOLVED.value}]
                res = self.backend.session.rput('incidents', json=payload)
                if res:
                    record = res[0]

            # record = self.session.find("incidents", instance.incident_key, attribute='incident_key')
            # if not record:
            #     record = self.session.rpost("incidents", json=payload, headers={'from': self.server.sender_id})

            response_body = record
            api_response = DetailAPIResponse(status_code=200, response_message='OK', is_valid=True,
                                             remote_id=record['id'], remote_status=record['status'],
                                             response_body=response_body)
        except PDClientError as e:
            api_response = self._proccess_exception(e)

        api_response.request_body = payload
        return api_response


class PDPriorityIntegrator(PDIntegrator):

    def get_remote_objects(self, request_data=None) -> ListAPIResponse:
        return self._get_object_list('priorities')

    def save_local_object(self, remote_object):
        return Category.objects.get_or_create(
                site=self.backend.site,
                type=KalertsK.CAT_PRIORITY.value,
                remote_id=get_dotted_dict(remote_object, 'id'),
                defaults={
                    'name': get_dotted_dict(remote_object, 'summary'),
                    'remote_name': get_dotted_dict(remote_object, 'summary'),
                    'identifier': slugify(get_dotted_dict(remote_object, 'summary')),
                    'date_modified': parse_date(get_dotted_dict(remote_object, 'last_status_change_at')),
                    'date_created': parse_date(get_dotted_dict(remote_object, 'created_at'))
                })[0]


class PDUserIntegrator(PDIntegrator):

    def get_remote_objects(self, request_data=None) -> ListAPIResponse:
        return self._get_object_list('users')

    def save_local_object(self, remote_object):
        email = get_dotted_dict(remote_object, 'email')
        if email:
            user = get_user_model().objects.get_or_create(
                username=email,
                defaults={
                    'last_name': get_dotted_dict(remote_object, 'name'),
                })[0]

            return RemoteUser.objects.get_or_create(
                site=self.backend.site,
                remote_id=get_dotted_dict(remote_object, 'id'),
                defaults={
                    'user': user
                })[0]


class PagerDutyBackend(BaseBackend):
    identifier = KalertsK.BACKEND_PAGER_DUTY.value
    name = 'Pager Duty'
    base_url = 'https://api.pagerduty.com'
    integrator_classes = {
        KalertsK.SYNC_SECTION_INCIDENT.value: PDIncidentIntegrator,
        KalertsK.SYNC_SECTION_PRIORITY.value: PDPriorityIntegrator,
        KalertsK.SYNC_SECTION_USER.value: PDUserIntegrator,
    }

    def get_session(self):
        api_token = self.get_token()
        session = APISession(api_token)
        try:
            session.subdomain
        except PDClientError:
            session = None
        return session
