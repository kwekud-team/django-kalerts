from django.contrib.sites.models import Site

from kalerts.models import Incident
from kalerts.constants import KalertsK
from kalerts.contrib.sync.backends.pager_duty import PagerDutyBackend


class SyncUtils:
    MAP = {
        KalertsK.BACKEND_PAGER_DUTY.value: PagerDutyBackend
    }

    def group_by_site(self, queryset):
        unique_site_pks = set(queryset.values_list('site', flat=True))
        xs = []
        for pk in unique_site_pks:
            xs.append({
                'site': Site.objects.filter(pk=pk).first(),
                'queryset': queryset.filter(site__pk=pk)
            })

        return xs

    def get_backend_class(self, site):
        return PagerDutyBackend  # TODO: Get default backend from django settings

        # server = Server.objects.filter(site=site, in_use=True).first()
        # if server:
        #     backend_class = self.MAP.get(server.identifier)
        #
        # return backend_class

    def sync_local_to_remote(self, sections, queryset=None):
        queryset = queryset or Incident.objects.all()
        xs = []
        for dt in self.group_by_site(queryset):
            backend = self.get_backend_class(dt['site'])(dt['site'])
            integrators = backend.get_integrators(sections)

            for key, integrator in integrators.items():
                synced = integrator.push_local_to_remote(dt['queryset'])
                xs.append({
                    'key': key,
                    'site': dt['site'],
                    'synced_count': len(synced),
                    'synced_records': synced
                })

        return xs

    def sync_remote_to_local(self, site, sections):
        xs = []
        backend = self.get_backend_class(site)(site)
        integrators = backend.get_integrators(sections)

        for key, integrator in integrators.items():
            synced = integrator.pull_remote_to_local()
            xs.append({
                'key': key,
                'site': site,
                'synced_count': len(synced),
                'synced_records': synced
            })
        return xs
