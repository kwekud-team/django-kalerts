from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone

from kalerts.dclasses import AlertBuilder, AlertItem
from kalerts.models import Category, Incident, IncidentUser
from kalerts.constants import KalertsK
from kalerts.dclasses import IncidentItem


class IncidentUtils:

    def __init__(self, site):
        self.site = site

    def get_service(self, identifier):
        return Category.objects.get_or_create(
            site=self.site,
            type=KalertsK.CAT_SERVICE.value,
            identifier=identifier,
            defaults={
                'name': identifier
            })[0]

    def log_incident(self, incident_item: IncidentItem):
        service = self.get_service(incident_item.service_identifier)

        if incident_item.priority_identifier:
            priority = Category.objects.filter(site=self.site, type=KalertsK.CAT_PRIORITY.value,
                                               identifier=incident_item.priority_identifier).first()
        else:
            priority = None

        # if incident_item.instances:
        #     app_model = get_str_from_model(incident_item.instances.model)
        #     pks = incident_item.instances.values_list('pk', flat=True)
        #     pks_str = ','.join(str(x) for x in pks)
        #     pks_cnt = len(pks)
        # else:
        #     app_model, pks_str, pks_cnt = '', '', 0

        if service:
            content_type = ContentType.objects.get_for_model(incident_item.ref_instance)
            incident = Incident.objects.update_or_create(
                site=self.site,
                service=service,
                incident_key=incident_item.key,
                defaults={
                    'title': incident_item.title,
                    'from_user': incident_item.from_user,
                    'priority': priority,
                    'body': incident_item.description,
                    'type': incident_item.type,
                    'urgency': incident_item.urgency,
                    'status': KalertsK.STATUS_TRIGGERED.value,
                    'action_url': incident_item.action_url,
                    'ref_object_pk': incident_item.ref_instance.pk,
                    'ref_content_type': content_type,
                })[0]

            for assignment in incident_item.assignments:
                IncidentUser.objects.get_or_create(
                    site=self.site,
                    incident=incident,
                    user=assignment.user,
                    defaults={
                        'is_responsible': assignment.is_responsible,
                        'group_label': assignment.group_label
                    })

            # sync
            incident_qs = Incident.objects.filter(id=incident.pk)
            self.sync_incident(incident_qs)

            return incident

    def complete_incident(self, incident_keys, user, status):
        incident_keys = map(str, incident_keys)

        qs = Incident.objects.filter(site=self.site, incident_key__in=incident_keys)
        qs.update(date_resolved=timezone.now(), resolved_by=user, status=status)

        # sync
        self.sync_incident(qs)

    def sync_incident(self, incident_qs):
        sync_run_type = getattr(settings, KalertsK.SYNC_RUN_VARIABLE.value, None)
        if sync_run_type == KalertsK.SYNC_RUN_CELERY.value:
            from kalerts.tasks import bgr_sync_local_to_remote

            pks = ','.join(map(str, incident_qs.values_list('pk', flat=True)))
            bgr_sync_local_to_remote.delay(pks)

        elif sync_run_type == KalertsK.SYNC_RUN_CELERY.value:
            from kalerts.contrib.sync.utils import SyncUtils

            SyncUtils().sync_local_to_remote([KalertsK.SYNC_SECTION_INCIDENT.value], incident_qs)
        else:
            # Do nothing. Just save so it can be run with management command later.
            pass


class AlertUtils:

    def __init__(self, site):
        self.site = site

    def get_site_alerts(self, limit_queryset=5):
        total_count = 0

        xs = []
        for x in Category.objects.visible().filter(site=self.site, type=KalertsK.CAT_SERVICE.value):
            incident_qs = Incident.objects.pending().filter(site=self.site, service=x)
            cnt = incident_qs.count()
            xs.append(
                AlertItem(key=x.pk, name=x.name, count=cnt, queryset=incident_qs[:limit_queryset]),
            )
            total_count += cnt

        return AlertBuilder(
            total_count=total_count,
            alert_items=xs
        )
