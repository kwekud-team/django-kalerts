from django.urls import path

from kalerts import views


app_name = 'kalerts'


urlpatterns = [
    path("sync/remote/<int:site_pk>/", views.SyncRemoteView.as_view(), name="sync_remote"),
]
