from django.contrib import messages
from django.views.generic.edit import FormView

from kalerts.forms import SyncRemoteForm
from kalerts.contrib.sync.utils import SyncUtils


class SyncRemoteView(FormView):
    template_name = 'kalerts/sync_remote.html'
    form_class = SyncRemoteForm

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw['request'] = self.request
        return kw

    def get_initial(self):
        return {'site': self.kwargs['site_pk']}

    def form_valid(self, form):
        site = form.cleaned_data['site']
        sections = form.cleaned_data['sections']

        dt = SyncUtils().sync_remote_to_local(site, sections)
        # total_count = dt.get('synced_count', 0)

        messages.success(self.request, f'Synced successfully')

        return super().form_valid(form)

    def get_success_url(self):
        redir = self.request.META.get('HTTP_REFERER', None) or '/'
        if '?next=' in redir:
            redir = redir.split('?next=')[1]

        return redir
