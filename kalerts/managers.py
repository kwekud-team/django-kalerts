from django.db import models


class CategoryManager(models.Manager):

    def visible(self):
        qs = super().get_queryset()
        return qs.filter(is_active=True, is_visible=True)


class IncidentManager(models.Manager):

    def pending(self):
        qs = super().get_queryset()
        return qs.filter(date_resolved__isnull=True)
