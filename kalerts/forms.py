from django import forms
from django.contrib.sites.models import Site

from kommons.utils.http import get_request_site
from kalerts.constants import KalertsK


SECTIONS = (
    (KalertsK.SYNC_SECTION_USER.value, KalertsK.SYNC_SECTION_USER.value.upper()),
    (KalertsK.SYNC_SECTION_SERVICE.value, KalertsK.SYNC_SECTION_SERVICE.value.upper()),
    (KalertsK.SYNC_SECTION_PRIORITY.value, KalertsK.SYNC_SECTION_PRIORITY.value.upper()),
    (KalertsK.SYNC_SECTION_INCIDENT.value, KalertsK.SYNC_SECTION_INCIDENT.value.upper()),
)


class SyncRemoteForm(forms.Form):
    site = forms.ModelChoiceField(queryset=Site.objects.all())
    sections = forms.MultipleChoiceField(choices=SECTIONS, widget=forms.CheckboxSelectMultiple())

    def __init__(self, **kwargs):
        request = kwargs.pop('request')
        super().__init__(**kwargs)

        self.fields['site'].initial = get_request_site(request)
        self.fields['sections'].initial = request.GET.getlist('section')
